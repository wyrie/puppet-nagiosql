require 'spec_helper'

describe 'nagiosql', :type => :class do
  context "on a Debian OS" do
    let :facts do
      {
        :osfamily               => 'Debian',
        :operatingsystem        => 'Debian',
        :operatingsystemrelease => '7',
        :lsbdistcodename        => 'wheezy',
        :kernel                 => 'Linux',
        :concat_basedir         => '/dne', # Required by concat module, dependency of apache module.
        :root_home              => '/root',
      }
    end
    let :default_params do
      {
        :download_url     => 'http://sourceforge.net/projects/nagiosql/files/latest/download',
        :htdocs_path      => '/nsql',
        :admin_password   => 'test',
        :mysql_dbhost     => 'test.puppet.local',
        :mysql_dbname     => 'testdb',
        :mysql_dbuser     => 'testuser',
        :mysql_dbpassword => 'testpassword',
      }
    end

    let(:params) {default_params}

    it { is_expected.to contain_class("nagiosql::params") }

    it { is_expected.to contain_wget__fetch("nagiosql_download").with(
      'source'      => 'http://sourceforge.net/projects/nagiosql/files/latest/download',
      'destination' => '/tmp/nagiosql.tar.gz',
      'timeout'     => 0,
      'verbose'     => false,
      )
    }

    it { is_expected.to contain_exec("nagiosql_extract").with(
      'command' => 'tar zxf /tmp/nagiosql.tar.gz -C /nsql && mv `find /nsql -mindepth 1 -maxdepth 1 -type d | grep -i nagiosql` /nsql/nagiosql',
      'creates' => '/nsql/nagiosql',
      'before'  => 'File[/nsql/nagiosql]',
      'require' => 'Wget::Fetch[nagiosql_download]',
      )
    }

    it { is_expected.to contain_file('/nsql/nagiosql').with(
      'ensure'  => 'directory',
      'recurse' => true,
      'purge'   => false,
      'force'   => false,
      'owner'   => 'www-data',
      'group'   => 'www-data',
      )
    }

    it { is_expected.to contain_mysql__db('testdb').with(
      'ensure'   => 'present',
      'user'     => 'testuser',
      'password' => 'testpassword',
      'host'     => 'test.puppet.local',
      )
    }

    it { is_expected.to contain_exec('nagiosql_dbimport').with(
      'command'   => "mysql --defaults-file=/root/.my.cnf testdb < /nsql/nagiosql/install/\$(sed -rn 's/\\\$preSqlNewInstall[^\"]+\"(.*)\";/\\1/p' /nsql/nagiosql/install/install.php)",
      'unless'    => "mysql --defaults-file=/root/.my.cnf testdb -Be \"SHOW TABLES LIKE 'tbl_settings'\" | grep '^tbl_settings'",
      'logoutput' => true,
      ).that_requires(
        'Mysql::Db[testdb]'
      ).that_requires(
        'File[/nsql/nagiosql]'
      )
    }

    it { is_expected.to contain_exec('nagiosql_sqlsettings').with(
        'logoutput'   => true,
        'refreshonly' => true,
      ).that_subscribes_to(
        'Exec[nagiosql_dbimport]'
      )
    }

    it { is_expected.to contain_file('/nsql/nagiosql/config/settings.php').with(
      'ensure'  => 'file',
      'replace' => false,
      'owner'   => 'www-data',
      'group'   => 'www-data',
      'require' => 'File[/nsql/nagiosql]',
      )
    }

    it { is_expected.to contain_file('/nsql/nagiosql/install').with(
      'ensure'  => 'absent',
      'force'   => true,
      'require' => 'Exec[nagiosql_dbimport]',
      )
    }

    [
      '/etc/nagiosql',
      '/etc/nagiosql/hosts',
      '/etc/nagiosql/services',
      '/etc/nagiosql/backup',
      '/etc/nagiosql/backup/hosts',
      '/etc/nagiosql/backup/services'
    ].each do |nsqldir|
      it { is_expected.to contain_file(nsqldir).with(
        'ensure' => 'directory',
        'owner'  => 'www-data',
        'group'  => 'nagios',
        'mode'   => '6755',
        'purge'  => false,
        'force'  => false,
        )
      }
    end

    [
      '/etc/nagiosql/timeperiods.cfg',
      '/etc/nagiosql/commands.cfg',
      '/etc/nagiosql/contacts.cfg',
      '/etc/nagiosql/contactgroups.cfg',
      '/etc/nagiosql/contacttemplates.cfg',
      '/etc/nagiosql/hosttemplates.cfg',
      '/etc/nagiosql/hostgroups.cfg',
      '/etc/nagiosql/hostextinfo.cfg',
      '/etc/nagiosql/hostescalations.cfg',
      '/etc/nagiosql/hostdependencies.cfg',
      '/etc/nagiosql/servicetemplates.cfg',
      '/etc/nagiosql/servicegroups.cfg',
      '/etc/nagiosql/serviceextinfo.cfg',
      '/etc/nagiosql/serviceescalations.cfg',
      '/etc/nagiosql/servicedependencies.cfg'
    ].each do |nsqlfile|
      it { is_expected.to contain_file(nsqlfile).with(
        'ensure'  => 'present',
        'replace' => false,
        'owner'   => 'www-data',
        'group'   => 'nagios',
        )
      }
    end

    context "with $http_manage => true" do
      let :params do
        default_params.merge({
          :http_manage => true
        })
      end
      it { is_expected.to contain_class("apache") }
    end

    context "with $php_manage => true" do
      let :params do
        default_params.merge({
          :http_manage => true,
          :php_manage => true
        })
      end
      it { is_expected.to contain_class("apache::mod::php") }

      [
        'php5-mcrypt',
        'php5-intl',
        'php-gettext',
        'php5-curl',
        'php-xml-parser',
        'php5-mysql',
        'php-pear',
        'libssh2-php'
      ].each do |modname|
        it { is_expected.to contain_package(modname).with(
          'ensure' => 'present'
        )}
      end
    end

    context "with $http_vhost => false" do
      it { is_expected.to contain_apache__custom_config('nagiosql_phpdirectives').with(
        'content' => "<Location /nagiosql>\nphp_admin_flag file_uploads on\nphp_value session.auto_start \"0\"\n</Location>",
        )
      }
    end

    context "with $http_vhost => true" do
      let :params do
        default_params.merge({
          :http_manage     => true,
          :http_vhost      => true,
          :http_servername => 'test.puppet.local'
        })
      end

      it { is_expected.to contain_apache__vhost('nagiosql').with(
        'ensure'          => 'present',
        'servername'      => 'test.puppet.local',
        'port'            => 80,
        'priority'        => '80',
        'docroot'         => '/nsql/nagiosql',
        'php_admin_flags' => {
          'file_uploads' => 'on',
        },
        'custom_fragment' => "\nphp_admin_flag file_uploads on\nphp_value session.auto_start \"0\"\n",
        'require'         => 'File[/nsql/nagiosql]',
        )
      }
    end

    context "with $mysql_manage => true" do
      let :params do
        default_params.merge({
          :mysql_manage     => true,
        })
      end

      it { is_expected.to contain_class('mysql::server') }
    end

    context "with $nagios_manage => true" do
      let :params do
        default_params.merge({
          :nagios_manage     => true,
        })
      end

      [
        'nagios3',
        'nagios-plugins'
      ].each do |pkg|
        it { is_expected.to contain_package(pkg).with(
          'ensure' => 'present'
        )}
      end

      it { is_expected.to contain_file('/etc/nagios3').with(
        'group'   => 'www-data',
        'mode'    => '0775',
        'require' => 'Package[nagios3]',
        )
      }

      [
        '/etc/nagios3/nagios.cfg',
        '/etc/nagios3/cgi.cfg'
      ].each do |nfile|
        it { is_expected.to contain_file(nfile).with(
          'group'   => 'www-data',
          'mode'    => '0664',
          'require' => 'Package[nagios3]',
          )
        }
      end

      it { is_expected.to contain_file_line('check_external_commands').with(
        'path'    => '/etc/nagios3/nagios.cfg',
        'line'    => 'check_external_commands=1',
        'match'   => '^(#)?check_external_commands=[0-1]',
        'require' => 'Package[nagios3]',
        'notify'  => 'Service[nagios3]',
        )
      }

      [
        'timeperiods',
        'commands',
        'contacts',
        'contactgroups',
        'contacttemplates',
        'hosttemplates',
        'hostgroups',
        'hostextinfo',
        'hostescalations',
        'hostdependencies',
        'servicetemplates',
        'servicegroups',
        'serviceextinfo',
        'serviceescalations',
        'servicedependencies'
      ].each do |fl|
        it { is_expected.to contain_file_line("nagiosql_#{fl}").with(
          'path'    => '/etc/nagios3/nagios.cfg',
          'line'    => "cfg_file=/etc/nagiosql/#{fl}.cfg",
          'notify'  => 'Service[nagios3]',
          ).that_requires(
            'Package[nagios3]'
          ).that_requires(
            "File[/etc/nagiosql/#{fl}.cfg]"
          )
        }
      end

      [
        'hosts',
        'services',
      ].each do |fl|
        it { is_expected.to contain_file_line("nagiosql_#{fl}").with(
          'path'    => '/etc/nagios3/nagios.cfg',
          'line'    => "cfg_dir=/etc/nagiosql/#{fl}",
          'notify'  => 'Service[nagios3]',
          ).that_requires(
            'Package[nagios3]'
          ).that_requires(
            "File[/etc/nagiosql/#{fl}]"
          )
        }
      end

      it { is_expected.to contain_service('nagios3').with(
        'ensure'    => true,
        'enable'    => true,
        'hasstatus' => false,
        'pattern' => '/usr/sbin/nagios3'
        )
      }

      it { is_expected.to contain_file('/usr/sbin/nagios3').with(
        'owner'   => 'nagios',
        'group'   => 'www-data',
        'mode'    => '0750',
        'require' => 'Package[nagios3]',
        )
      }

      it { is_expected.to contain_exec('perms_nagios_cmdfile').with(
          'command'   => "dpkg-statoverride --update --add nagios www-data 2710 `dirname /var/lib/nagios3/rw/nagios.cmd` && dpkg-statoverride --update --add  nagios nagios 751  $(dirname $(dirname /var/lib/nagios3/rw/nagios.cmd))",
          'unless'    => "test 'www-data' = \"`stat -c '%G' /var/lib/nagios3/rw/nagios.cmd`\"",
          'logoutput' => true,
          'notify'    => 'Service[nagios3]',
        )
      }

      it { is_expected.to contain_file('/var/run/nagios3').with(
          'owner'   => 'nagios',
          'group'   => 'www-data',
          'mode'    => '0754',
          'require' => 'Package[nagios3]',
        )
      }
    end
  end

  context "on a RedHat OS" do
    let :facts do
      {
        :osfamily               => 'RedHat',
        :operatingsystem        => 'RedHat',
        :operatingsystemrelease => '6',
        :kernel                 => 'Linux',
        :concat_basedir         => '/dne', # Required by concat module, dependency of apache module.
        :root_home              => '/root',
      }
    end
    let :default_params do
      {
        :download_url     => 'http://sourceforge.net/projects/nagiosql/files/latest/download',
        :htdocs_path      => '/nsql',
        :admin_password   => 'test',
        :mysql_dbhost     => 'test.puppet.local',
        :mysql_dbname     => 'testdb',
        :mysql_dbuser     => 'testuser',
        :mysql_dbpassword => 'testpassword',
      }
    end

    context "with $php_manage => true" do
      let :params do
        default_params.merge({
          :http_manage => true,
          :php_manage => true
        })
      end
      it { is_expected.to contain_class("apache::mod::php") }

      [
        'php-mcrypt',
        'php-intl',
        'php-php-gettext',
        'php-xml',
        'php-mysql',
        'php-pear',
        'php-pecl-ssh2'
      ].each do |modname|
        it { is_expected.to contain_package(modname).with(
          'ensure' => 'present'
        )}
      end
    end

    context "with $nagios_manage => true" do
      let :params do
        default_params.merge({
          :nagios_manage     => true,
        })
      end

      [
        'nagios',
        'nagios-plugins-all'
      ].each do |pkg|
        it { is_expected.to contain_package(pkg).with(
          'ensure' => 'present'
        )}
      end

      it { is_expected.to contain_file('/etc/nagios').with(
        'group'   => 'apache',
        'mode'    => '0775',
        'require' => 'Package[nagios]',
        )
      }

      [
        '/etc/nagios/nagios.cfg',
        '/etc/nagios/cgi.cfg'
      ].each do |nfile|
        it { is_expected.to contain_file(nfile).with(
          'group'   => 'apache',
          'mode'    => '0664',
          'require' => 'Package[nagios]',
          )
        }
      end

      it { is_expected.to contain_service('nagios').with(
        'ensure'    => true,
        'enable'    => true,
        'hasstatus' => false,
        'pattern' => '/usr/sbin/nagios'
        )
      }

      it { is_expected.to contain_file('/usr/sbin/nagios').with(
        'owner'   => 'nagios',
        'group'   => 'apache',
        'mode'    => '0750',
        'require' => 'Package[nagios]',
        )
      }

      it { is_expected.to contain_exec('perms_nagios_cmdfile').with(
          'command'   => "chown nagios:apache /var/spool/nagios/cmd/nagios.cmd",
          'unless'    => "test 'apache' = \"`stat -c '%G' /var/spool/nagios/cmd/nagios.cmd`\"",
          'logoutput' => true,
          'subscribe' => 'Service[nagios]',
        )
      }
    end
  end
end
