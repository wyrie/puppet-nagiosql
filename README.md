#NagiosQL

![Build Status](https://img.shields.io/bitbucket/pipelines/wyrie/puppet-nagiosql.svg)
[![Puppet Forge](https://img.shields.io/puppetforge/v/wyrie/nagiosql.svg)](https://forge.puppetlabs.com/wyrie/nagiosql)

####Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with nagiosql](#setup)
    * [Beginning with nagiosql](#beginning-with-nagiosql)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

##Overview

This module will install and configure an instance of NagiosQL on your system.

##Module Description

The module can optionally manage the requirements of NagiosQL (apache, php, mysql and nagios) by setting the various parameter values. When managing nagios with this module you'll get a complete nagiosql configuration integrated with a default installation of nagios. If you decide to manage nagios from another module you'll need to add the nagios.cfg cfg_file and cfg_dir values yourself and set the appropriate permissions for the nagios binary and command file. See http://www.nagiosql.org/documentation.html#Installation.

The same goes for mysql and php. If you already manage php and apache's mod_php do not set it here but check the installation guide for required settings. Not managing mysql from this module will still create the required database and schema for you.

##Setup

###Beginning with nagiosql

```puppet
class { 'nagiosql':
  admin_password => 'somesecret'
}
```
This will install into /var/www/nagiosql, create a database with permissions and set various configuration files. This requires:
- a apache server with php enabled.
- mysql server on localhost.
- nagios installed, configured and nagiosql manually integrated or with another module (see: http://forge.puppetlabs.com/thias/nagios)


##Usage

### Installed as a virtual host

```puppet
class { 'nagiosql':
  htdocs_path     => '/vhosts',
  http_vhost      => true,
  http_servername => 'nagiosql.mydomain.com',
  admin_password  => 'test1234',
  nagios_manage   => true,
}
```

This will install into /vhosts/nagiosql, create a database with permissions and set various configuration files. It will also
install nagios and handle the nagiosql integration with it. This would probably be the best configuration for you if you intend
to use nagiosql to configure your entire nagios environment.


### I don't care just pile it on
```puppet
class { 'nagiosql':
  mysql_manage    => true,
  php_manage      => true,
  nagios_manage   => true,
  admin_password  => 'test12345',
}
```

This will give you a complete working nagiosql environment out of the box. Great for testing, but you'll probably want more control over your
other modules than what's given here.

##Reference

###Classes

####Public Classes

* nagiosql: Main class, includes all other classes.

####Private Classes

* nagiosql::params: Default values.

###Parameters

The following parameters are available in the nagiosql module:

####`download_url`
Url from which to fetch the nagiosql sources.

####`htdocs_path`
The path from where your web servers serves files. This
path will be expanded to htdocs_path/nagiosql. If http_vhost is true
then the docroot of the vhost is htdocs_path/nagiosql

####`http_manage`
Boolean. If true will declare the apache class. Enable for a nagiosql
only install. In most cases you'll want this on the default value of
false

####`http_vhost`
Boolean. If true will install nagiosql as an apache

virtual host.
####`http_servername`
Apache servername directive.i Required if http_vhost is true.

####`http_port`
Apache listening port (defaults to port 80). Required if http_vhost is true.

####`http_ssl`
Boolean. If true will tell nagiosql to use https.

####`admin_username`
Username for the nagiosql administrative account (defaults to admin).

####`admin_password`
Required password for the administrative account.

####`locale`
Nagiosql support a few locales for it's interface. If you locale is
supported you can set it here.

####`php_manage`
Boolean. If true will 1) enable mod_php 2) install required php modules
and 3) set required php.ini settings.

####`php_timezone`
The value of date.timezone in php.ini. Defaults to the facter timezone value
which may or may not be suitable to your installation.

####`mysql_manage`
Boolean. If true will install a mysql server on localhost and generate
a random password for root.

####`mysql_dbhost`
Location of the mysql server (defaults to localhost).

####`mysql_dbname`
Name of database to create for nagiosql (defaults to db_nagiosql)

####`mysql_dbuser`
Mysql user with access to the nagiosql database.

####`mysql_dbpassword`
Required password value for the mysql user account.

####`nagios_manage`
Boolean. If true will:
1) install nagios and manage the nagios service
2) set values in nagios.cfg and
3) set the appropriate file permissions for the nagios binary and command file.

####`nagiosql_confdir`
Absolute path to the directory that contains the nagios
configuration files written by nagiosql.

####`ensure`
Present or absent.

##Limitations

This module has been built on and tested against Puppet 3.3.2 and higher.

The module has been tested on:

* CentOS 6
* Debian 7

##Development

1. [Fork](http://help.github.com/forking/) puppet-nagiosql
2. Create a topic branch against the develop branch `git checkout develop; git checkout -b my_branch`
3. Make sure you have added tests for your changes. Tests are written with [rspec-puppet](http://rspec-puppet.com/).
4. Run all the tests to assure nothing else was accidentally broken. To run all tests: rake spec
5. Push to your branch `git push origin my_branch`
6. Create a [Pull Request](http://help.github.com/pull-requests/) from your branch against the develop branch.

