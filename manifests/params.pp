# == Class: nagiosql::params
#
# Configuration settings.
#
# === Authors
#
# Scott Barr <scott@barr.co.za>
#
class nagiosql::params {
  $source_path = '/tmp/nagiosql.tar.gz'
  $nagios_user = 'nagios'

  case $::osfamily {
    'Debian': {
      $htdocs_user = 'www-data'
      $php_modules = ['php5-mcrypt', 'php5-intl', 'php-gettext', 'php5-curl', 'php-xml-parser', 'php5-mysql', 'php-pear', 'libssh2-php']
      $nagios_package = 'nagios3'
      $nagios_package_plugins = 'nagios-plugins'
      $nagios_configdir = '/etc/nagios3'
      $nagios_binary = '/usr/sbin/nagios3'
      $nagios_command_file = '/var/lib/nagios3/rw/nagios.cmd'
      $nagios_rundir = '/var/run/nagios3'
      $nagios_lock_file = "${nagios_rundir}/nagios3.pid"
    }
    'Redhat': {
      $htdocs_user = 'apache'
      $php_modules = ['php-mcrypt', 'php-intl', 'php-php-gettext', 'php-xml', 'php-mysql', 'php-pear', 'php-pecl-ssh2']
      $nagios_package = 'nagios'
      $nagios_package_plugins = 'nagios-plugins-all'
      $nagios_configdir = '/etc/nagios'
      $nagios_binary = '/usr/sbin/nagios'
      $nagios_command_file = '/var/spool/nagios/cmd/nagios.cmd'
      $nagios_rundir = '/var/run'
      $nagios_lock_file = "${nagios_rundir}/nagios.pid"
    }
    default: {
      fail("${::operatingsystem} is not supported.")
    }
  }
}
