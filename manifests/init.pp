# == Class: nagiosql
#
# Installs and configures NagiosQL on Debian and Redhat systems.
#
# === Parameters
#
# [*download_url*]
#   Url from which to fetch the nagiosql sources.
#
# [*htdocs_path*]
#   The path from where your web servers serves files. This
#   path will be expanded to htdocs_path/nagiosql. If http_vhost is true
#   then the docroot of the vhost is htdocs_path/nagiosql
#
# [*http_manage*]
#   Boolean. If true will declare the apache class. Enable for a nagiosql
#   only install. In most cases you'll want this on the default value of
#   false
#
# [*http_vhost*]
#   Boolean. If true will install nagiosql as an apache
#   virtual host.
#
# [*http_servername*]
#   Apache servername directive.i Required if http_vhost is true.
#
# [*http_port*]
#   Apache listening port (defaults to port 80). Required if http_vhost is true.
#
# [*http_ssl*]
#   Boolean. If true will tell nagiosql to use https.
#
# [*admin_username*]
#   Username for the nagiosql administrative account (defaults to admin).
#
# [*admin_password*]
#   Required password for the administrative account.
#
# [*locale*]
#   Nagiosql support a few locales for it's interface. If you locale is
#   supported you can set it here.
#
# [*php_manage*]
#   Boolean. If true will 1) enable mod_php 2) install required php modules
#   and 3) set required php.ini settings.
#
# [*php_timezone*]
#   The value of date.timezone in php.ini. Defaults to the facter timezone value
#   which may or may not be suitable to your installation.
#
# [*mysql_manage*]
#   Boolean. If true will install a mysql server on localhost and generate
#   a random password for root.
#
# [*mysql_dbhost*]
#   Location of the mysql server (defaults to localhost).
#
# [*mysql_dbname*]
#   Name of database to create for nagiosql (defaults to db_nagiosql)
#
# [*mysql_dbuser*]
#   Mysql user with access to the nagiosql database.
#
# [*mysql_dbpassword*]
#   Required password value for the mysql user account.
#
# [*nagios_manage*]
#   Boolean. If true will:
#   1) install nagios and manage the nagios service
#   2) set values in nagios.cfg and
#   3) set the appropriate file permissions for the nagios binary and command file.
#
# [*nagiosql_confdir*]
#   Absolute path to the directory that contains the nagios
#   configuration files written by nagiosql.
#
# [*ensure*]
#   Present or absent.
#
# === Examples
#
#  See tests folder.
#
# === Authors
#
# Scott Barr <gsbarr@gmail.com>
#
class nagiosql (
  $download_url     = 'http://sourceforge.net/projects/nagiosql/files/latest/download',
  $htdocs_path      = '/var/www',
  $http_manage      = false,
  $http_vhost       = false,
  $http_servername  = '',
  $http_port        = '80',
  $http_ssl         = false,
  $admin_username   = 'admin',
  $admin_password   = '',
  $locale           = 'en_GB',
  $php_manage       = false,
  $php_timezone     = undef,
  $mysql_manage     = false,
  $mysql_dbhost     = 'localhost',
  $mysql_dbname     = 'db_nagiosql',
  $mysql_dbuser     = 'nagiosql_user',
  $mysql_dbpassword = undef,
  $nagios_manage    = false,
  $nagiosql_confdir = '/etc/nagiosql',
  $ensure           = present,
) {
  include nagiosql::params

  validate_bool($http_manage)
  validate_bool($http_vhost)
  validate_bool($http_ssl)
  validate_bool($php_manage)
  validate_bool($mysql_manage)
  validate_bool($nagios_manage)

  if empty($admin_password) {
    fail('The admin password parameter is required and it may not be empty')
  }

  validate_absolute_path($htdocs_path)

  if $htdocs_path =~ /\/$/ {
    $install_path = "${htdocs_path}nagiosql"
  } else {
    $install_path = "${htdocs_path}/nagiosql"
  }

  validate_re($ensure, '^(present|absent)$',
  'ensure parameter must have a value of: present or absent')

  if $http_vhost and empty($http_servername) {
    fail('The servername parameter is required when installing as a virtualhost')
  }

  if $http_manage {
    class { 'apache':
      mpm_module => 'prefork',
    }
  }

  if $php_manage {
    class { 'apache::mod::php':
    }
    package { $nagiosql::params::php_modules:
      ensure => $ensure,
    }
  }

  Exec {
    path    => '/usr/bin:/bin:/usr/sbin:/usr/local/bin',
  }

  if $ensure == 'present' {
    wget::fetch { 'nagiosql_download':
      source      => $download_url,
      destination => $nagiosql::params::source_path,
      timeout     => 0,
      verbose     => false,
    }
    exec { 'nagiosql_extract':
      command => "tar zxf ${nagiosql::params::source_path} -C ${htdocs_path} && mv `find ${htdocs_path} -mindepth 1 -maxdepth 1 -type d | grep -i nagiosql` ${install_path}",
      creates => $install_path,
      before  => File[$install_path],
      require => Wget::Fetch['nagiosql_download'],
    }
  }

  $purge = $ensure ? {
    absent  => true,
    default => false,
  }

  $directory_ensure = $ensure ? {
    present => directory,
    default => $ensure,
  }

  file { $install_path:
    ensure  => $directory_ensure,
    recurse => true,
    purge   => $purge,
    force   => $purge,
    owner   => $nagiosql::params::htdocs_user,
    group   => $nagiosql::params::htdocs_user,
  }

  if $http_vhost {
    $base_url = '/'
    # Older versions of the apache module only support enabling mod_ssl and configuration of the vhost
    # would require a custom template. Later version support ssl parameters but not the template.
    # If you require SSL define your requirement outside of this module and tell nagiosql what you decided.
    $real_http_ssl = false

    apache::vhost { 'nagiosql':
      ensure          => $ensure,
      servername      => $http_servername,
      port            => $http_port,
      priority        => '80',
      docroot         => $install_path,
      php_admin_flags => {
        'file_uploads' => 'on',
      },
      custom_fragment => template('nagiosql/phpdirectives.erb'),
      require         => File[$install_path],
    }
  } else {
    $base_url = '/nagiosql/'
    $real_http_ssl = $http_ssl

    apache::custom_config { 'nagiosql_phpdirectives':
      content => template('nagiosql/phpdirectives.erb'),
    }
  }

  $real_dbpassword = $mysql_dbpassword ? {
    'UNSET' => sha1("${::uniqueid}dfldkjr@87fD"),
    default => $mysql_dbpassword,
  }

  if $mysql_manage {
    $mysql_enabled = $ensure ? {
      present => true,
      default => false,
    }

    class { 'mysql::server':
      package_ensure   => $ensure,
      service_enabled  => $mysql_enabled,
      root_password    => sha1("${::uniqueid}${::fqdn}"),
      override_options => {
        'mysqld' => {
          'bind_address' => '127.0.0.1',
        },
      },
    }
  }

  mysql::db { $mysql_dbname:
    ensure   => $ensure,
    user     => $mysql_dbuser,
    password => $real_dbpassword,
    host     => $mysql_dbhost,
    grant    => ['SELECT','INSERT','UPDATE','DELETE','LOCK TABLES'],
  }

  if $ensure == 'present' {
    exec { 'nagiosql_dbimport':
      command   => "mysql --defaults-file=${::root_home}/.my.cnf ${mysql_dbname} < ${install_path}/install/\$(sed -rn 's/\\\$preSqlNewInstall[^\"]+\"(.*)\";/\\1/p' ${install_path}/install/install.php)",
      unless    => "mysql --defaults-file=${::root_home}/.my.cnf ${mysql_dbname} -Be \"SHOW TABLES LIKE 'tbl_settings'\" | grep '^tbl_settings'",
      logoutput => true,
      require   => [ Mysql::Db[$mysql_dbname], File[$install_path] ],
    }
    exec { 'nagiosql_sqlsettings':
      command     => template('nagiosql/settings_sql.erb'),
      logoutput   => true,
      refreshonly => true,
      subscribe   => Exec['nagiosql_dbimport'],
    }

    file { "${install_path}/config/settings.php":
      ensure  => file,
      replace => false,
      owner   => $nagiosql::params::htdocs_user,
      group   => $nagiosql::params::htdocs_user,
      content => template('nagiosql/settings_php.erb'),
      require => File[$install_path],
    }
    file { "${install_path}/install":
      ensure  => absent,
      force   => true,
      require => Exec['nagiosql_dbimport'],
    }
    }

  if $nagios_manage {
    package { [$nagiosql::params::nagios_package, $nagiosql::params::nagios_package_plugins]:
      ensure => $ensure,
    }

    file { $nagiosql::params::nagios_configdir:
      group   => $nagiosql::params::htdocs_user,
      mode    => '0775',
      require => Package[$nagiosql::params::nagios_package],
    }

    file { ["${nagiosql::params::nagios_configdir}/nagios.cfg", "${nagiosql::params::nagios_configdir}/cgi.cfg"]:
      group   => $nagiosql::params::htdocs_user,
      mode    => '0664',
      require => Package[$nagiosql::params::nagios_package],
    }

    # Use file_line as suppose to concat or templates so that we
    # don't fight with nagiosql for the configuration file and only
    # maintain the minimum we need.
    File_line {
      ensure  => $ensure,
      path    => "${nagiosql::params::nagios_configdir}/nagios.cfg",
      notify  => Service[$nagiosql::params::nagios_package],
    }
    file_line { 'check_external_commands':
      line    => 'check_external_commands=1',
      match   => '^(#)?check_external_commands=[0-1]',
      require => Package[$nagiosql::params::nagios_package],
    }
    file_line { 'nagiosql_timeperiods':
      line    => "cfg_file=${nagiosql_confdir}/timeperiods.cfg",
      require => [ Package[$nagiosql::params::nagios_package], File["${nagiosql_confdir}/timeperiods.cfg"] ],
    }
    file_line { 'nagiosql_commands':
      line    => "cfg_file=${nagiosql_confdir}/commands.cfg",
      require => [ Package[$nagiosql::params::nagios_package], File["${nagiosql_confdir}/commands.cfg"] ],
    }
    file_line { 'nagiosql_contacts':
      line    => "cfg_file=${nagiosql_confdir}/contacts.cfg",
      require => [ Package[$nagiosql::params::nagios_package], File["${nagiosql_confdir}/contacts.cfg"] ],
    }
    file_line { 'nagiosql_contactgroups':
      line    => "cfg_file=${nagiosql_confdir}/contactgroups.cfg",
      require => [ Package[$nagiosql::params::nagios_package], File["${nagiosql_confdir}/contactgroups.cfg"] ],
    }
    file_line { 'nagiosql_contacttemplates':
      line    => "cfg_file=${nagiosql_confdir}/contacttemplates.cfg",
      require => [ Package[$nagiosql::params::nagios_package], File["${nagiosql_confdir}/contacttemplates.cfg"] ],
    }
    file_line { 'nagiosql_hosts':
      line    => "cfg_dir=${nagiosql_confdir}/hosts",
      require => [ Package[$nagiosql::params::nagios_package], File["${nagiosql_confdir}/hosts"] ]
    }
    file_line { 'nagiosql_hosttemplates':
      line    => "cfg_file=${nagiosql_confdir}/hosttemplates.cfg",
      require => [ Package[$nagiosql::params::nagios_package], File["${nagiosql_confdir}/hosttemplates.cfg"] ],
    }
    file_line { 'nagiosql_hostgroups':
      line    => "cfg_file=${nagiosql_confdir}/hostgroups.cfg",
      require => [ Package[$nagiosql::params::nagios_package], File["${nagiosql_confdir}/hostgroups.cfg"] ],
    }
    file_line { 'nagiosql_hostextinfo':
      line    => "cfg_file=${nagiosql_confdir}/hostextinfo.cfg",
      require => [ Package[$nagiosql::params::nagios_package], File["${nagiosql_confdir}/hostextinfo.cfg"] ],
    }
    file_line { 'nagiosql_hostescalations':
      line    => "cfg_file=${nagiosql_confdir}/hostescalations.cfg",
      require => [ Package[$nagiosql::params::nagios_package], File["${nagiosql_confdir}/hostescalations.cfg"] ],
    }
    file_line { 'nagiosql_hostdependencies':
      line    => "cfg_file=${nagiosql_confdir}/hostdependencies.cfg",
      require => [ Package[$nagiosql::params::nagios_package], File["${nagiosql_confdir}/hostdependencies.cfg"] ],
    }
    file_line { 'nagiosql_services':
      line    => "cfg_dir=${nagiosql_confdir}/services",
      require => [ Package[$nagiosql::params::nagios_package], File["${nagiosql_confdir}/services"] ],
    }
    file_line { 'nagiosql_servicetemplates':
      line    => "cfg_file=${nagiosql_confdir}/servicetemplates.cfg",
      require => [ Package[$nagiosql::params::nagios_package], File["${nagiosql_confdir}/servicetemplates.cfg"] ],
    }
    file_line { 'nagiosql_servicegroups':
      line    => "cfg_file=${nagiosql_confdir}/servicegroups.cfg",
      require => [ Package[$nagiosql::params::nagios_package], File["${nagiosql_confdir}/servicegroups.cfg"] ],
    }
    file_line { 'nagiosql_serviceextinfo':
      line    => "cfg_file=${nagiosql_confdir}/serviceextinfo.cfg",
      require => [ Package[$nagiosql::params::nagios_package], File["${nagiosql_confdir}/serviceextinfo.cfg"] ],
    }
    file_line { 'nagiosql_serviceescalations':
      line    => "cfg_file=${nagiosql_confdir}/serviceescalations.cfg",
      require => [ Package[$nagiosql::params::nagios_package], File["${nagiosql_confdir}/serviceescalations.cfg"] ],
    }
    file_line { 'nagiosql_servicedependencies':
      line    => "cfg_file=${nagiosql_confdir}/servicedependencies.cfg",
      require => [ Package[$nagiosql::params::nagios_package], File["${nagiosql_confdir}/servicedependencies.cfg"] ],
    }

    $ensure_bool = $ensure ? {
      present => true,
      default => false,
    }

    service { $nagiosql::params::nagios_package:
      ensure    => $ensure_bool,
      enable    => $ensure_bool,
      hasstatus => false,
      pattern   => $nagiosql::params::nagios_binary,
    }

    file { $nagiosql::params::nagios_binary:
      owner   => $nagiosql::params::nagios_user,
      group   => $nagiosql::params::htdocs_user,
      mode    => '0750',
      require => Package[$nagiosql::params::nagios_package],
    }

    case $::osfamily {
      'Debian': {
        # The nagios command file is created when nagios starts and
        # when the check_external_commands directive is enabled.
        # Make sure we set the required permissions before the service starts.
        exec { 'perms_nagios_cmdfile':
          command   => "dpkg-statoverride --update --add ${nagiosql::params::nagios_user} ${nagiosql::params::htdocs_user} 2710 `dirname ${nagiosql::params::nagios_command_file}` && dpkg-statoverride --update --add  ${nagiosql::params::nagios_user} ${nagiosql::params::nagios_user} 751  $(dirname $(dirname ${nagiosql::params::nagios_command_file}))",
          unless    => "test '${nagiosql::params::htdocs_user}' = \"`stat -c '%G' ${nagiosql::params::nagios_command_file}`\"",
          logoutput => true,
          notify    => Service[$nagiosql::params::nagios_package],
        } #TODO - the path check is a bit hacky.

        # Nagiosql requires access to the pid file to determine
        # if the daemon is running. Ensure read access on Debian
        # systems.
        file { $nagiosql::params::nagios_rundir:
          owner   => $nagiosql::params::nagios_user,
          group   => $nagiosql::params::htdocs_user,
          mode    => '0754',
          require => Package[$nagiosql::params::nagios_package],
        }
      }
      'RedHat': {
        # The nagios command file is created when nagios runs and
        # has the check_external_commands directive enabled. Make
        # sure we set the required permissions after everything.
        exec { 'perms_nagios_cmdfile':
          command   => "chown ${nagiosql::params::nagios_user}:${nagiosql::params::htdocs_user} ${nagiosql::params::nagios_command_file}",
          unless    => "test '${nagiosql::params::htdocs_user}' = \"`stat -c '%G' ${nagiosql::params::nagios_command_file}`\"",
          logoutput => true,
          subscribe => Service[$nagiosql::params::nagios_package],
        }
      }
      default: {
        notify { "If you've added support for ${::operatingsystem} you'll need to extend this case statement to.":
        }
      }
    }
  }

  file { [$nagiosql_confdir, "${nagiosql_confdir}/hosts", "${$nagiosql_confdir}/services", "${$nagiosql_confdir}/backup",
  "${$nagiosql_confdir}/backup/hosts", "${$nagiosql_confdir}/backup/services"]:
    ensure => $directory_ensure,
    owner  => $nagiosql::params::htdocs_user,
    group  => $nagiosql::params::nagios_user,
    mode   => '6755',
    purge  => $purge,
    force  => $purge,
  }
  # Create empty default configuration files so that the nagios service doesn't complain about missing files.
  file { ["${nagiosql_confdir}/timeperiods.cfg","${nagiosql_confdir}/commands.cfg","${nagiosql_confdir}/contacts.cfg",
  "${nagiosql_confdir}/contactgroups.cfg","${nagiosql_confdir}/contacttemplates.cfg","${nagiosql_confdir}/hosttemplates.cfg",
  "${nagiosql_confdir}/hostgroups.cfg","${nagiosql_confdir}/hostextinfo.cfg","${nagiosql_confdir}/hostescalations.cfg",
  "${nagiosql_confdir}/hostdependencies.cfg","${nagiosql_confdir}/servicetemplates.cfg","${nagiosql_confdir}/servicegroups.cfg",
  "${nagiosql_confdir}/serviceextinfo.cfg","${nagiosql_confdir}/serviceescalations.cfg","${nagiosql_confdir}/servicedependencies.cfg"]:
    ensure  => $ensure,
    replace => false,
    owner   => $nagiosql::params::htdocs_user,
    group   => $nagiosql::params::nagios_user,
  }
}
